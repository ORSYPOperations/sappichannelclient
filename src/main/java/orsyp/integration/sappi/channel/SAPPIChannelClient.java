package orsyp.integration.sappi.channel;

import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class SAPPIChannelClient {

	public static void main(String[] args) throws Exception {
		if (args.length!=3) {
			System.out.println("Usage:\nSAPPIChannelControl <user> <password> <url>");
			return;
		}				
		String user = args[0];
		String pw = args[1];
		String url = args[2];
		String host = new URL(url).getHost();
		int port = new URL(url).getPort();
		if (port==-1)
			port=80;
		
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(new AuthScope(host, port), new UsernamePasswordCredentials(user, pw));
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		try {
			HttpGet httpget = new HttpGet(url);

			System.out.println("Requesting " + httpget.getRequestLine());
			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				HttpEntity entity = response.getEntity();
				
				System.out.println(response.getStatusLine());
				if (entity != null)
					System.out.println("Response content length: " + entity.getContentLength());
				System.out.println("-------------- Response contents --------------------------");
				java.util.Scanner s = new java.util.Scanner(entity.getContent()).useDelimiter("\\A");
				String content =  s.hasNext() ? s.next() : "";
				s.close();
				System.out.println(content);
				EntityUtils.consume(entity);
			} finally {
				response.close();
			}
		} finally {
			httpclient.close();
		}
	}
}
